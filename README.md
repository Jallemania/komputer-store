# Komputer Store

## Name
Assignment 1: Javascript fundamentals.

## Description
A JavaScript only HACKER_LIFE simulator where you can work, take out loans and buy computers.

## Installation
Just clone the repository and run 'npm install'.

## Support
Contact me for support on GitLab @Jallemania

## Contributing
No need for contributions since this project is perfect(all things are relative, hehe)

## Project status
Awaiting grade
