//ELEMENTS
const bankAmountElement = document.getElementById("balance");
const loanAmountElement = document.getElementById("loanAmount");
const salaryAmountElement = document.getElementById("salary");
const loanElement = document.getElementById("loanDiv");
const bankButtonElement = document.getElementById("bankBtn");
const payButtonElement = document.getElementById("payBankBtn");
const workButtonElement = document.getElementById("workBtn");
const payAllButtonElement = document.getElementById("payAllBtn");
const laptopSelectElement = document.getElementById("laptopSelect");
const featureListElement = document.getElementById("featureList");
const komputerInfoElement = document.getElementById("komputerInfo");
const komputerImgElement = document.getElementById("komputerImg");
const komputerTitleElement = document.getElementById("komputerTitle");
const komputerDescElement = document.getElementById("komputerDesc");
const komputerPriceElement = document.getElementById("komputerPrice");
const buyNowBtnElement = document.getElementById("buyNowBtn");

//GLOBAL VARIABLES
let bankBalance = 0.0;
let loanBalance = 0.0;
let salaryBalance = 0.0;
let hasLoan = false;
let lapTops = [];

//API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => lapTops = data)
    .then(() => addLaptopsToMenu());

//FUNCTIONS

//Calls functions to populate laptop-list
const addLaptopsToMenu = () => {
    lapTops.forEach(x => addLaptopToSelect(x));
    lapTops[0].specs.forEach(x => addFeatureList(x));
    changeLaptopInfo(lapTops[0])
};

//Creates laptop options for select-element
const addLaptopToSelect = (lapTop) => {
    const optionElement = document.createElement("option");
    optionElement.value = lapTop.id;
    optionElement.appendChild(document.createTextNode(lapTop.title));
    laptopSelectElement.appendChild(optionElement);
};

//Call necessary functions to change laptop, gets the selected laptop
const handleLaptopChange = e => {
    removeChildren(featureListElement);
    const selectedLaptop = lapTops[e.target.value - 1];
    selectedLaptop.specs.forEach(x => addFeatureList(x));
    changeLaptopInfo(selectedLaptop);  
};

//Creates li-elements and appends them to feature-list
const addFeatureList = (spec) => {
    const liElement = document.createElement("li");
    liElement.innerText = spec;
    featureListElement.appendChild(liElement);
};

//Takes in a laptop object and changes information based on that in the Info Section
const changeLaptopInfo = (laptop) => {
    //komputerImgElement
    komputerTitleElement.innerText = laptop.title;
    komputerDescElement.innerText = laptop.description;
    komputerPriceElement.innerText = `${laptop.price} kr`;
    changeImage(laptop);
    buyNowBtnElement.value = (laptop.id - 1);
};

//Changes src tag to change image
const changeImage = (laptop) => {
    if(laptop.id === 5){
        komputerImgElement.src = `https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png`;
    }else{
        komputerImgElement.src = `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`;
    }
}

//Checks for outstanding loan and changes element visibilities. Also Updates balances.
const checkLoan = () => {
    if(loanBalance > 0){
        hasLoan = true;
        loanElement.className = "balanceText visible";
        payAllButtonElement.className = "visible btn";
    }else{
        hasLoan = false;
        loanElement.className = "balanceText hidden";
        payAllButtonElement.className = "hidden btn";
    };

    if(bankBalance > 0){
        bankButtonElement.className = "visible btn";
    }else{
        bankButtonElement.className = "hidden btn";
    }
    
    bankAmountElement.innerText = `${bankBalance} kr`;
    loanAmountElement.innerText = `${loanBalance} kr`;
    salaryAmountElement.innerText = `${salaryBalance} kr`;
};

//Takes input from user and returns said amount.
const handleBankPrompt = () => {
    let loanAmount = prompt("How much would you like to loan?", "Only numbers");
    parseInt(loanAmount);
    
    while(isNaN(loanAmount)){
        loanAmount = prompt("How much would you like to loan?", "You can only input numbers!")
        parseInt(loanAmount);
    };
    
    return loanAmount;
};

//Checks if user input meets loan requirements and adds loan to balances.
const getLoan = () => {
    let loanAmount = handleBankPrompt();
    loanAmount = parseInt(loanAmount);
    
    if(loanAmount <= (bankBalance * 2)) {
        bankBalance += loanAmount;
        loanBalance = loanAmount;
        bankAmountElement.innerText = bankBalance;
    }else{
        alert("You are not eligible for that amount.");
    };
    
    checkLoan();
};

//Adds money to salary balance
const makeMoney = () => {
    salaryBalance += 100;
    checkLoan();
};

//Takes salary balance and adds it to bank balances. Resets salary balance.
const bankSalary = () => {   
    if (hasLoan) {
        let salaryPercent = salaryBalance / 10;
        bankBalance += (salaryPercent * 9);
        loanBalance -= salaryPercent;
    }else{
        bankBalance += salaryBalance;
    };
    
    salaryBalance = 0;
    checkLoan();
};

//Takes salary balance and checks if it is larger than the loan and if so puts the difference in the bank
const payLoan = () => {
    if(salaryBalance <= loanBalance){
        loanBalance -= salaryBalance;
    }else{
        let difference = salaryBalance - loanBalance;
        bankBalance += difference;
        loanBalance = 0.0;
    };
    
    salaryBalance = 0.0;
    checkLoan();
}

//Checks if the selected computer price is lower than bank balance. Subtracts price from bank balance.
const buyKomputer = e => {
    let selectedLaptop = lapTops[e.target.value];

    if(selectedLaptop.price <= bankBalance){
        bankBalance -= selectedLaptop.price;
        checkLoan();
        alert(`CONGRATULATIONS! You now own a ${selectedLaptop.title}`);
    }else{
        alert("You do not have sufficient funds.");
    }
};

//Removes all children from a parent element
const removeChildren = (parent) => {
    while (parent.lastChild) {
        parent.removeChild(parent.lastChild);
    };
};


//EVENT LISTENERS
bankButtonElement.addEventListener("click", getLoan);
workButtonElement.addEventListener("click", makeMoney);
payButtonElement.addEventListener("click", bankSalary);
payAllButtonElement.addEventListener("click", payLoan);
laptopSelectElement.addEventListener("change", handleLaptopChange);
buyNowBtnElement.addEventListener("click", buyKomputer);